import { Selector } from 'testcafe';

export default class Signup {
    constructor () {
        this.homePage = 'http://web-testbox.cp.local/';
        this.signupPage = 'http://web-testbox.cp.local/signup';

        this.trynowButton = Selector('#main-try-now');

        this.teamSectionTitle = Selector('.team .title');
        this.teamField = Selector('#signup-team');
        this.teamFieldValue = 'Automation Test Team';
        this.teamFieldIncorrectValue = 'err';
        this.teamFieldWarning = Selector('.form-error-text');
        this.teamSubmit = Selector('#signup-team-submit');

        this.backNavigation = Selector('.back-navigation');

        this.emailSectionTitle = Selector('.email .title');
        this.emailField = Selector('#signup-email');
        this.emailFieldValue = 'automation@automation.com';
        this.emailFieldIncorrectValue = 'erremail';
        this.emailFieldWarning = Selector('.form-error-text');
        this.emailSubmit = Selector('#signup-email-submit');

        this.passwordSectionTitle = Selector('.password .title');
        this.passwordField = Selector('#signup-password');
        this.passwordFieldValue_LC = 'auto';
        this.passwordFieldValue_LCUC = 'Auto';
        this.passwordFieldValue_LCUCNO = 'Auto123';
        this.passwordFieldValue = 'Auto1234';
        this.passwordFieldWarning = Selector('.form-error-text .message-error');
      //  this.emailSubmit = Selector('#signup-password-submit');
    }
}