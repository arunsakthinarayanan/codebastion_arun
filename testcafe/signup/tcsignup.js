import Signup from './tcsignupmodel';
import {ClientFunction, t} from 'testcafe';
import { Selector } from 'testcafe';

const signup = new Signup();

fixture `TestCafe Sign Up`
    .page (signup.homePage)
    .beforeEach( async t => {
        await t
        .setTestSpeed(0.25)
        .resizeWindow(200, 100)
        .maximizeWindow()
    });
   /* .after(async t =>{
        await t
        .resizeWindowToFitDevice('Samsung Galaxy S4 I9500', {
            portraitOrientation: true
        })
        .resizeWindowToFitDevice('iPad', {
            portraitOrientation: false
        })
    }); */

test
    ('sign up as a new user', async t => {
    const getURL = ClientFunction(() => document.location.href);
    await t
        .expect(getURL()).eql(signup.homePage) 
        .setTestSpeed(.5)
        .click(signup.trynowButton)

        .expect(getURL()).eql(signup.signupPage)
        
        .expect((signup.teamSectionTitle).exists).ok()
        .click(signup.teamSubmit)
        .expect(signup.teamFieldWarning.exists).ok()
        .typeText(signup.teamField, signup.teamFieldIncorrectValue)
        .click(signup.teamSubmit)
        .expect((signup.teamFieldWarning).exists).ok()
        .selectText(signup.teamField).pressKey('backspace') 
        .typeText(signup.teamField, signup.teamFieldValue)
        .click(signup.teamSubmit);

        const titleContents = await Selector('.email .title').textContent;
    await t
        .expect((signup.emailSectionTitle).exists).ok()
        .expect(titleContents).contains('Lead your team.')
        .click(signup.backNavigation)
        .expect((signup.backNavigation).exists).notOk()
        .expect(signup.teamField.value).eql(signup.teamFieldValue)
        .click(signup.teamSubmit)
        .click(signup.emailSubmit)
        .expect((signup.emailFieldWarning).exists).ok('This is assertion passed message')
        .typeText(signup.emailField, signup.emailFieldIncorrectValue)
        .click(signup.emailSubmit)
        .expect((signup.emailFieldWarning).exists).ok() 
        .typeText(signup.emailField,signup.emailFieldValue,{ replace: true })
        .click(signup.emailSubmit);

    await t
        .expect((signup.passwordSectionTitle).exists).ok()
        .typeText(signup.passwordField, signup.passwordFieldValue_LC)
        .selectText(signup.passwordField).pressKey('backspace')

    const warningElements = signup.passwordFieldWarning.filterVisible(); 
    const warningElementsCount = await warningElements.count;
    let warningMessage = ''; 

    //iterating through a set here

    for (let i=0; i<warningElementsCount; i++){
        warningMessage = warningMessage+' '+await warningElements.nth(i).textContent;
    }
    console.log('here is the warning message'+':'+warningMessage);
    
});

//test opens in the below specified url
test 
    .page (signup.homePage)
    //test level hook overrides fixture hook
    .before( async t => {
     await t
     .expect((signup.trynowButton).exists).ok();
    })
    .skip('Screenshot bad test', async t => {
    await t 
    //the step below will fail
        .expect((signup.teamSectionTitle).exists).ok();
});

test
    .page ('http://cgi-lib.berkeley.edu/ex/fup.html')
    .skip ('Uploading', async t => {
    await t
        .setFilesToUpload(document.input[upfile], [
            './uploads/1.jpg',
            './uploads/2.jpg',
            './uploads/3.jpg'
        ])
        .click('#upload-button');
});