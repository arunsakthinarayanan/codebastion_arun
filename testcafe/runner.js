const createTestCafe = require('testcafe');
let testcafe         = null;

createTestCafe('localhost', 1337, 1338)
    .then(tc => {
        testcafe     = tc;
        const runner = testcafe.createRunner();

        return runner
            .src(['tcsignup.js'])
            .browsers(['chrome'])
            .concurrency(1)
            .screenshots('results',true)
            .run({selectorTimeout: 10000,
                assertionTimeout: 10000,
                speed: 1,
                pageloadTimeout: 10000,

            });
    })
    .then(failedCount => {
        console.log('Tests failed: ' + failedCount);
        testcafe.close();
    })
    .catch(error =>{
        console.log (error);
    });